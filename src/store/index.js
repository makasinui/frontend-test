import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    products: [],
  },
  getters: {
    products: (state) => state.products,
  },
  mutations: {
    addProduct(state, item) {
      const lastID = state.products.length ? state.products.at(-1).id + 1 : 1;
      state.products.push({ ...item, id: lastID });
    },
    deleteProduct(state, id) {
      const product = state.products.findIndex((i) => i.id === id);
      state.products.splice(product, 1);
    },
    sortProducts(state, field) {
      switch (field) {
        case 'price-min':
          state.products.sort((a, b) => {
            const first = a.price;
            const second = b.price;

            return first - second;
          });
          break;
        case 'price-max':
          state.products.sort((a, b) => {
            const first = a.price;
            const second = b.price;

            return second - first;
          });
          break;
        case 'name':
          state.products.sort((a, b) => {
            const first = a.title.toLowerCase();
            const second = b.title.toLowerCase();

            if (first < second) {
              return -1;
            }
            if (first > second) {
              return 1;
            }

            return 0;
          });
          break;
        default:
      }
    },
  },
  actions: {
    addProduct({ commit }, item) {
      commit('addProduct', item);
    },
    deleteProduct({ commit }, id) {
      commit('deleteProduct', id);
    },
  },
});

export default store;
